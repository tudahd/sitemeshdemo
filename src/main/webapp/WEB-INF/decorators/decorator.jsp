<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.1 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<meta name="decorator" content="main">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><decorator:title default="SiteMesh Demo"/></title>
</head>
<body id="page-home">
<div id="page">
    <div id="header" class="clearfix">
        HEADER
        <hr />
    </div>

    <div id="content" class="clearfix">
        <div id="main">
            <h3>Main Content Decorator</h3>
            <decorator:body />
            <hr />
        </div>
    <div id="footer" class="clearfix">
        Footer
    </div>

</div>

<div id="extra1">&nbsp;</div>
<div id="extra2">&nbsp;</div>
</body>
</html>
